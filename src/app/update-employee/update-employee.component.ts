import { Component, OnInit } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { Http,Response,Headers } from '@angular/http';
import { ActivatedRoute} from '@angular/router';
import { Router } from '@angular/router';
import 'rxjs/add/operator/toPromise'
import { Content } from '@angular/compiler/src/render3/r3_ast';
import { FormsModule} from '@angular/forms'
import { NgModule } from '@angular/core'

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {

  id:number;
  employeeobj:object={};
  public employees=[];
  data:object={};
  private headers=new Headers({'Content-Type':'application/json'});

  constructor(private http:Http,private router:Router,private route:ActivatedRoute) { }

  updateemployee(emp)
  {
  this.employeeobj={
"name":emp.name,
"address":emp.address,
"mobile":emp.mobile,
"email":emp.email
  }
  const Url="http://localhost:5555/employees"+this.id;
  this.http.put(Url,JSON.stringify(this.employeeobj),{headers:this.headers}).toPromise()
  .then(() =>
  {
this.router.navigate(['/'])
  });
  }

  ngOnInit() {


  this.route.params.subscribe(params =>{
  this.id = +params['id'];
  console.log("id ",this.id);
  this.fetchData(this.id);
});

// let url="http://localhost:5555/employees/"+this.id+"";
// console.log(url);
//  this.http.get(url).subscribe
// ((res:Response)=>{
//   this.employees = res.json();
//   debugger;
//   console.log(" emp ",this.employees);
//   // for(var i=0; i < this.employees.length; i++)
//   // if(parseInt(this.employees[i].id)===this.id){
//   //   this.data = this.employees[i];
//   // }
// }
// )
  }

fetchData = function(id)
{
  let url="http://localhost:5555/employees/"+id+"";
console.log(url);
 // this.http.get("http://localhost:5555/employees").subscribe((res:Response)=>
 this.http.get(url).subscribe((res:Response)=>
  {
    this.employees=res.json();
console.log(this.employees);
this.data=this.employees;
console.log("data",this.data.id);
  })
}
}
