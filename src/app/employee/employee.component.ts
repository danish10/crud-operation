import { Component, OnInit } from '@angular/core';
import { Http,Response,Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { error } from 'protractor';
import { EmployeeService } from '../employee.service';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  public employees=[];
  public id:number;

private headers=new Headers({'Content-Type':'application/json'});

  constructor(private empService:EmployeeService,private http: Http) { }



  ngOnInit() {
      //this.getEmployee();
    this.fetchData();
  }

// getEmployee()
// {
//   this.empService.getEmployee().subscribe(data =>{
//     console.log(data);
//     this.employees=data;
    
//   }, error => {
   
//   });
// }

// function()
// {
//   this.http.get("./assets/employee.json").subscribe(
//     (res:Response) =>
//     {
//       this.employees=res.json();
//       console.log(this.employees)
//     }
//   )
// }

fetchData = function()
{
  this.http.get("http://localhost:5555/employees").subscribe((res:Response)=>
  {
    this.employees=res.json();
console.log(this.employees);
  })
}


deleteEmployee = function(id)
{
  if(confirm("are you sure")){
 //const url =`${"http://localhost:5555/employees"}/${id}`;
 const url ="http://localhost:5555/employees/"+id;
 console.log(url);
 return this.http.delete(url,{headers:this.headers}).toPromise().then (() =>{
   this.fetchData();
})
  }
}

products:any=[
  {id:0,name:"Danish"}
]
}
