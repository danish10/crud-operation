import { Component, OnInit } from '@angular/core';
import { Http,Response,Headers } from '@angular/http';
import { EmployeeComponent } from '../employee/employee.component';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
employeeobj:object={};
public isAdded:boolean=false;
public confirmation:string ="New Employee Added succusfully";

  constructor(private http:Http) {
    
   }
   addemployee = function(emp)
 {
  this.employeeobj={
    "name":emp.name,
    "mobile":emp.mobile,
    "address":emp.address,
    "email":emp.email
  }
   this.http.post("http://localhost:5555/employees",this.employeeobj).subscribe((res:Response)=>
   {
    this.isAdded = true;
   })
 }


  ngOnInit() {
    
  }

}
