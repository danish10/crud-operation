import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { AppComponent } from './app.component';
import { EmployeeComponent } from './employee/employee.component';
import{ HttpModule } from '@angular/http';
import { FormsModule} from '@angular/forms'
import { from } from 'rxjs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HomeComponent } from './home/home.component';
import { UpdateEmployeeComponent } from './update-employee/update-employee.component';


@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    HomeComponent,
    UpdateEmployeeComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
   
    RouterModule.forRoot([
      
      {
        path:'',
        component:EmployeeComponent
             },
             {
               path:'add/Detail',
               component:HomeComponent
             },
             {
              path:'employee/update/:id',
              component:UpdateEmployeeComponent
            }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 

}
